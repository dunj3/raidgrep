raidgrep
========

[![CI status](https://img.shields.io/gitlab/pipeline/dunj3/raidgrep/master)](https://gitlab.com/dunj3/raidgrep)

A tool for searching your local Guild Wars 2/arcdps raid & fractal log files.

Building
--------

**raidgrep currently requires a nightly compiler** due to the `trait_alias`
feature!

* Compilation:
  * Debug build for developers: `cargo +nightly build` (warning: the resulting
    binary is slow!)
  * Release build: `cargo +nightly build --release`
* The build process can take a few minutes, as compiling
  [LALRPOP](https://github.com/lalrpop/lalrpop) and link-time-optimization will
  take a bit.
* The binary will be in `./target`
  * Debug binary: `./target/debug/raidgrep`
  * Release binary: `./target/release/raidgrep`

### Binaries

Pre-compiled binaries for each release can be found at
https://kingdread.de/raidgrep or alternatively attached to the [Gitlab
releases](https://gitlab.com/dunj3/raidgrep/-/releases).

### Packages

* Arch Linux (pre-compiled package):
  [`raidgrep-bin`](https://aur.archlinux.org/packages/raidgrep-bin)
* Arch Linux (git package):
  [`raidgrep-git`](https://aur.archlinux.org/packages/raidgrep-git)

Usage
-----

You can view the help message with `raidgrep --help`.

A manpage containing more information is available in
[raidgrep.1.asciidoc](raidgrep.1.asciidoc). You can convert it to the manpage
format by running `a2x -f manpage raidgrep.1.asciidoc` or to a HTML version by
running `asciidoc raidgrep.1.asciidoc`.

License
-------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
