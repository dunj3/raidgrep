# Changelog

All notable changes to this project will be documented in this file.

## Unreleased

## 1.5.0 - 2022-04-08
### Added
- Support for the End of Dragons specializations:
  - Willbender
  - Vindicator
  - Bladesworn
  - Mechanist
  - Untamed
  - Specter
  - Catalyst
  - Virtuoso
  - Harbinger
- Support for the End of Dragons strike missions:
  - Captain Mai Trin (shorthand "mai")
  - Ankka
  - Minister Li (shorthand "li")
  - The Dragonvoid (shorthand "dragonvoid")

## 1.4.0 - 2021-11-25
### Added
- The `-gamemode` predicate as well as its `-raid`, `-fractal`, `-strike`,
  `-golem` and `-wvw` shorthands.
- The "trio", "twisted castle", "river", "broken king", "eater" and "eyes"
  bosses.

### Changed
- Better error handling for situations in which the cache file could not be
  found.
- Better error handling if malformed archives are found during the scan.
- Better output for WvW logs.

### Fixed
- Fixed an underflow when parsing WvW logs (leading to a panic in debug mode).

## 1.3.1 - 2020-10-10
### Added
- The `--count`/`-n` flag to only output the number of matching logs.
- Support for Sunqua Peak logs.

## 1.3.0 - 2020-07-24
### Added
- The `--check` command line argument to check a single file.
- Comparison based filters (`count`, `-time`, `-duration`).
- Sorting based on fight duration.

### Changed
- The exit code will now be dependent on whether logs matching the filter were
  found or not.

### Fixed
- Fight outcomes for some logs (2nd/3rd/... kill in a week) have been fixed.

## 1.2.0 - 2020-05-16
### Added
- An indicator for whether a fight was done with Challenge Mote activated.
- A `-cm` filter to filter logs based on the Challenge Mote.
- The `--sort` command line argument to sort the output.

### Changed
- Boss names are now taken from `evtclib`
  - "Matthias" is now "Matthias Gabrel".
  - "Cairn" is now "Cairn the Indomitable".
  - "Desmina" is now "Soulless Horror".
  - "Largos Twins" is now "Twin Largos".
  - "Qadim The Peerless" is now "Qadim the Peerless".
  - "Skorvald" is now "Skorvald the Shattered".
- Regular expressions are now matched case-insensitive.
- Players in a subgroup are now sorted by their account names.

## 1.1.0 - 2020-05-04
### Added
- The REPL now remembers its history across program restarts.
- The `-log-before` and `-log-after` predicates, see the manpage for more
  information.
- The `-class` predicate to filter players based on their class.

### Changed
- Date based filters (-after and -before) will now use the filename to filter
  out logs early, speeding up the search.
- Filename output is not quoted anymore, and doesn't use double-backslashes on
  Windows.
- Ctrl-C will now only cancel the current search in the REPL instead of ending
  the program.

### Fixed
- Fixed timezone handling, so date output & filtering should now be consistent
  with the actual date.

## 1.0.2 - 2020-04-29
### Added
- Boss "kodans" for the Voice & Claw of the Fallen strike mission.
- More help text about the available boss names.

### Changed
- Updated `evtclib` to 0.2.0
  - With that, boss parsing fixes that were implemented there:
    "Soulless Horror", "Largos Twins", "Ensolyss of the Endless Torment",
    "Kodans", "Conjured Amalgamate"
- Boss names can now be quoted in the filter expression.

### Fixed
- Performance issue when -boss was being used, as it failed to filter logs with
  unknown bosses out early.

## 1.0.1 - 2020-04-26
### Changed
- Compilation is now done with link time optimization for the releases
