#!/bin/bash
set -e
echo "This script will compile and package raidgrep."
echo "Ensure that your working directory is in the desired state!"
echo "Press Enter to continue."
read

version() {
    grep -m 1 "^version =" Cargo.toml | cut -d '"' -f 2
}

output() {
    echo -e "\033[33m$1\033[0m"
}

VERSION=$(version)

output "Building $VERSION"
output "=> Linux build"
cargo build --release

output "=> Windows build"
cargo build --release --target=x86_64-pc-windows-gnu

output "=> Manpage"
a2x -f manpage raidgrep.1.asciidoc

output "=> HTML help"
asciidoc raidgrep.1.asciidoc

output "=> Packaging it up"
mkdir -p target/archive/

output "   Linux"
tar -czvf target/archive/raidgrep-$VERSION.tar.gz \
    raidgrep.1 \
    raidgrep.1.html \
    -C target/release/ raidgrep

output "   Windows"
zip -j target/archive/raidgrep-$VERSION.zip \
    raidgrep.1.html \
    target/x86_64-pc-windows-gnu/release/raidgrep.exe

output "   Checksums"
cd target/archive
sha256sum raidgrep-$VERSION.{tar.gz,zip} > raidgrep-$VERSION.sha256sum
