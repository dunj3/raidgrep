//! Module to resolve application-specific paths.
use super::APP_NAME;

use std::path::PathBuf;

/// Returns the path that should be used for the cache.
pub fn cache_path() -> Option<PathBuf> {
    dirs::cache_dir().map(|p| p.join(APP_NAME))
}

/// Returns the path that should be used for the REPL history.
pub fn history_path() -> Option<PathBuf> {
    let mut config_path = dirs::config_dir()?;
    config_path.push(APP_NAME);
    config_path.push("history");
    Some(config_path)
}
