use super::super::LogResult;
use super::{aggregators::Aggregator, formats::Format};

use std::{io::Write, sync::Mutex};

use anyhow::Result;

pub struct Pipeline {
    format: Box<dyn Format>,
    aggregator: Box<dyn Aggregator>,
    writer: Mutex<Box<dyn Write + Send>>,
}

impl Pipeline {
    pub fn new(
        writer: Box<dyn Write + Send>,
        format: Box<dyn Format>,
        aggregator: Box<dyn Aggregator>,
    ) -> Pipeline {
        Pipeline {
            format,
            aggregator,
            writer: Mutex::new(writer),
        }
    }

    pub fn push_item(&self, item: LogResult) -> Result<()> {
        let mut writer = self.writer.lock().unwrap();
        self.aggregator.push_item(item, &*self.format, &mut *writer)
    }

    pub fn finish(self) -> Result<()> {
        let mut writer = self.writer.lock().unwrap();
        self.aggregator.finish(&*self.format, &mut *writer)
    }
}
