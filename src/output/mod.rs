use super::{FightOutcome, LogResult, Opt};

use std::io;

pub mod aggregators;
pub mod formats;
pub mod pipeline;
pub mod sorting;

pub use self::pipeline::Pipeline;

use self::{aggregators::Aggregator, formats::Format};

/// Build an pipeline for the given command line options.
pub fn build_pipeline(opt: &Opt) -> Pipeline {
    let stream = io::stdout();
    let aggregator: Box<dyn Aggregator> = if opt.count {
        Box::new(aggregators::CountingOutput::new())
    } else if let Some(sorting) = &opt.sorting {
        Box::new(aggregators::SortedOutput::new(sorting.clone()))
    } else {
        Box::new(aggregators::WriteThrough)
    };

    let format: Box<dyn Format> = if opt.file_name_only {
        Box::new(formats::FileOnly)
    } else {
        Box::new(formats::HumanReadable {
            show_guilds: opt.guilds,
        })
    };

    Pipeline::new(Box::new(stream), format, aggregator)
}
