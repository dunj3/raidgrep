//! A crate defining different output formats for search results.
use std::fmt::Write;

use super::super::guilds;
use super::{FightOutcome, LogResult};

use chrono::Local;
use evtclib::GameMode;

/// An output format
pub trait Format: Sync + Send {
    /// Format a single log result
    fn format_result(&self, item: &LogResult) -> String;
}

/// The human readable, colored format.
#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq)]
pub struct HumanReadable {
    pub show_guilds: bool,
}

impl Format for HumanReadable {
    fn format_result(&self, item: &LogResult) -> String {
        use colored::Colorize;
        let mut result = String::new();

        writeln!(
            result,
            "{}: {}",
            "File".green(),
            item.log_file.to_string_lossy()
        )
        .expect("writing to String failed");

        if item.game_mode != Some(GameMode::WvW) {
            let outcome = match item.outcome {
                FightOutcome::Success => "SUCCESS".green(),
                FightOutcome::Wipe => "WIPE".red(),
            };
            writeln!(
                result,
                "{}: {} - {}: {}{} {} after {}",
                "Date".green(),
                item.time
                    .with_timezone(&Local)
                    .format("%Y-%m-%d %H:%M:%S %a"),
                "Boss".green(),
                item.encounter
                    .map(|x| x.to_string())
                    .unwrap_or_else(|| "unknown".into()),
                if item.is_cm { " CM" } else { "" },
                outcome,
                humantime::Duration::from(item.duration.to_std().unwrap()),
            )
            .expect("writing to String failed");
        } else {
            writeln!(
                result,
                "{}: {} - {} ended after {}",
                "Date".green(),
                item.time
                    .with_timezone(&Local)
                    .format("%Y-%m-%d %H:%M:%S %a"),
                "World vs. World".green(),
                humantime::Duration::from(item.duration.to_std().unwrap()),
            )
            .expect("writing to String failed");
        }

        for player in item.players.iter().filter(|p| !p.account_name.is_empty()) {
            write!(
                result,
                "  {:2} {:20} {:19} {:12}",
                player.subgroup,
                player.account_name.yellow(),
                player.character_name.cyan(),
                player.profession,
            )
            .expect("writing to String failed");
            if self.show_guilds {
                let guild = player.guild_id.as_ref().and_then(|id| guilds::lookup(id));
                if let Some(guild) = guild {
                    write!(
                        result,
                        " [{}] {}",
                        guild.tag().magenta(),
                        guild.name().magenta(),
                    )
                    .expect("writing to String failed");
                }
            }
            writeln!(result).expect("writing to String failed");
        }
        result
    }
}

/// A format which outputs only the file-name
#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq)]
pub struct FileOnly;

impl Format for FileOnly {
    fn format_result(&self, item: &LogResult) -> String {
        let filename = item.log_file.to_string_lossy();
        format!("{}\n", filename)
    }
}
